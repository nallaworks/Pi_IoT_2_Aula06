import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { TwdServiceProvider } from "../../providers/twd-service/twd-service";
import { Cardapio } from "../../models/episode";

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-details",
  templateUrl: "details.html"
})
export class DetailsPage {
  public id;
  public obg: any;
  public cardapio: Cardapio;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public twdService: TwdServiceProvider
  ) {
    this.id = navParams.get("id");
    this.cardapio = new Cardapio();

    this.twdService.getEpisodeById(this.id).then(data => {
      this.obg = data;
      this.cardapio.Pizza = this.obg.Pizza;
      this.cardapio.Preco = this.obg.Preco;
      this.cardapio.Ingredintes = this.obg.Ingredintes;
      this.cardapio.Imagem = this.obg.Imagem;
      console.log(this.cardapio);
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad DetailsPage");
  }
}
