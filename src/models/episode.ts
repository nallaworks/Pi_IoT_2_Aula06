export class Cardapio {
    
    constructor(
        public Pizza?: string,
        public Preco?: string,
        public Imagem?: string,
        public Ingredintes?: string) { }
}